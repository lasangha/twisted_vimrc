" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
"Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'
Plugin 'bling/vim-airline'
Plugin 'scrooloose/nerdtree.git'
"Plugin 'marijnh/tern_for_vim'

" all of your plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
""" END OF vundle

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['flow']

" Additional information
" Using https://github.com/sindresorhus/flow-bin for javascript

""" END OF syntastic

"Pathogen
execute pathogen#infect()

"NerdTree
map <C-o> :NERDTreeToggle<CR>
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

"TagList
map <C-t> :TlistToggle<CR>

"Looks
set tabstop=4
set softtabstop=4
set shiftwidth=4
"set textwidth=80
set smarttab

"Smart indentation, the one that takes care of things great
filetype indent on
set smartindent
"setlocal expandtab

"set expandtab!

"Visual
set laststatus=2

"Search
set incsearch 
set hlsearch 
set ignorecase
set smartcase

"Basic configuration
set number
set background=dark

"Navigation
map <C-Right> :tabn<CR>
map <C-Left> :tabp<CR>
map <A-Left> :bn<CR>
map <A-Right> :bp<CR>

"Save
map <F9> :w<CR>
imap <F9> <ESC>:w<CR>

" Tags
nmap <F8> :TagbarToggle<CR>

" Move lines
nnoremap <C-Down> :m .+1<CR>
noremap <C-Down> :m .+1<CR>
inoremap <C-Up> :m .-2<CR>
inoremap <C-Down> <Esc>:m .+1<CR>
inoremap <C-Up> <Esc>:m .-2<CR>
vnoremap <C-Down> :m '>+1<CR>
vnoremap <C-Up> :m '<-2<CR>
nnoremap <C-Up> :m .-2<CR>
inoremap <C-Down> <Esc>:m .+1<CR>
inoremap <C-Up> <Esc>:m .-2<CR>
vnoremap <C-Down> :m '>+1<CR>
vnoremap <C-Up> :m '<-2<CR>

" To work with drupal modules I need to use 2 spaces instead of tabs
function! Drupal()
	set softtabstop=2
	set shiftwidth=2
	set tabstop=2
	set expandtab
	retab
endfunction
